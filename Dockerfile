FROM arm32v7/node:8-slim
RUN mkdir /app
COPY . /app/
WORKDIR /app
RUN npm install

CMD [ "npm", "start" ]
